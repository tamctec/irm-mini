# IRM mini

![](docs/images/irm-mini-front.png)

Infinity RGB matrix mini, by TAMC


<a href="https://www.tindie.com/products/tamctec/irm-mini-8x8-rgb-matrix-3232mm/"><img src="https://d2ss6ovg47m0r5.cloudfront.net/badges/tindie-larges.png" alt="I sell on Tindie" width="200" height="104"></a>


## Introduction

Infinity RGB matrix mini, an 8 by 8 RGB matrix with possibility to extend "infinitely".

## Size

- Pitch 4.0mm
- Diameter: 32 x 32mm
- RGB: WS2812-2020

## Documentation

[View Documentation](https://tamc-irm-mini.readthedocs-hosted.com/en/latest/?)

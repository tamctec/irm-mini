import array
import rp2
from rp2 import PIO, StateMachine, asm_pio

@asm_pio(sideset_init=PIO.OUT_LOW, out_shiftdir=PIO.SHIFT_LEFT, autopull=True, pull_thresh=24)
def ws2812():
    T1 = 2
    T2 = 5
    T3 = 3
    label("bitloop")
    out(x, 1).side(0)[T3 - 1]
    jmp(not_x, "do_zero").side(1)[T1 - 1]
    jmp("bitloop").side(1)[T2 - 1]
    label("do_zero")
    nop().side(0)[T2 - 1]

class WS2812():
    ORDER = (1, 0, 2, 3)
    '''A class to control ws2812 RGB LED for RP2040 chip'''
    def __init__(self, pin, num):
        self.length = num
        self.pin = pin
        self.__sm__ = StateMachine(0, ws2812, freq=8000000, sideset_base=self.pin)
        self.__sm__.active(1)
        self.brightness = 100
        self.rgb_mode == self.RGB
        self.__buf__ = array.array("I", [0 for _ in range(self.length)])

    def __len__(self):
        return self.length

    def write(self):
        '''Write buffer to LEDs'''
        temp = [self.__brightness_handler__(color) for color in self.__buf__]
        temp = array.array("I", temp)
        self.__sm__.put(temp, 8)

    def write_all(self, value):
        for i in range(self.length):
            self.__buf__[i] = value
        self.write()

    def __color_to_hex__(self, color):
        if isinstance(color, (list, tuple)):
            color = [value[self.ORDER[0]], value[self.ORDER[1]], value[self.ORDER[2]]]
            color = (color[0] << 16) + (color[1] << 8) + color[2]
        return color

    def __hex_to_color___(self, color):
        r = (color >> 16) & 0xff
        g = (color >>  8) & 0xff
        b = (color >>  0) & 0xff
        value = [r, g, b]
        value = [value[self.ORDER[1]], value[self.ORDER[0]], value[self.ORDER[2]]]
        return value

    def __brightness_handler__(self, color):
        color = self.__hex_to_color___(color)
        color = [int(v / 255 * self.brightness) for v in color]
        return self.__color_to_hex__(color)

    def __getitem__(self, i):
        '''read led color buffer'''
        value = self.__hex_to_color___(self.__buf__[i])
        return tuple(value)

    def __setitem__(self, i, value*):
        '''Set led color buffer'''
        value = self.__color_to_hex__(value)
        self.__buf__[i] = value

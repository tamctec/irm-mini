from font_map import *
from sprite_map import Sprites
import time
import uos
import math

sysname = uos.uname()[0]
if sysname == "rp2":
    from ws2812 import WS2812
elif sysname in ["esp32", "esp8266"]:
    from neopixel import NeoPixel as WS2812
else:
    raise SystemError("Sorry, '%s' is not supported"%sysname)

class IRM():
    FONT7 = 0
    FONT5 = 1
    ZIGZAG = 0
    PROGRESSIVE = 1
    MATRIX_MODE = ZIGZAG
    
    def __init__(self, pin, pattern):
        self.width = len(pattern[0]) * 8
        self.height = len(pattern) * 8
        self.pattern = pattern
        self.n = self.width * self.height
        self.np = WS2812(pin, self.n)
        self.np.ORDER = (0, 1, 2)
        self.buffer = []
        self.font_list = (Font7(), Font5())
        self.sprites = Sprites()
        self._brightness = 255
        for y in range(self.height):
            line = []
            for x in range(self.width):
                line.append([0,0,0])
            self.buffer.append(line)

    @property
    def brightness(self):
        if sysname == "rp2":
            return self.np.brightness
        elif sysname in ["esp32", "esp8266"]:
            return self._brightness

    @brightness.setter
    def brightness(self, value):
        if sysname == "rp2":
            self.np.brightness = value
        elif sysname in ["esp32", "esp8266"]:
            self._brightness = value

    def clear(self):
        self.fill([0,0,0])

    def fill(self, color):
        color = self.color_handler(color)
        for y in range(self.height):
            for x in range(self.width):
                self.buffer[y][x] = color

    def draw_point(self, x, y, color):
        color = self.color_handler(color)
        if x < self.width and y < self.height:
            self.buffer[y][x] = color

    def draw_rect(self, x1, y1, x2, y2, color, fill=False):
        color = self.color_handler(color)
        # Top & Bottom line
        for x in range(x1, x2+1):
            if x > self.width or y1 > self.height:
                continue
            self.buffer[y1][x] = color
            if y2 > self.height:
                continue
            self.buffer[y2][x] = color
        # Left & Right line
        for y in range(y1+1, y2):
            if y > self.height or x1 > self.width:
                continue
            self.buffer[y][x1] = color
            if x2 > self.width:
                continue
            self.buffer[y][x2] = color
        # If Fill? draw fill
        if fill:
            for y in range(y1+1, y2):
                for x in range(x1+1, x2):
                    if x > self.width or y > self.height:
                        continue
                    self.buffer[y][x] = color

    # y = ax + b
    def draw_line(self, x1, y1, x2, y2, color):
        delta_x = x2 - x1
        delta_y = y2 - y1
        if delta_x == 0 and delta_y == 0:
            self.draw_point(x1, y1, color)
            return
        if delta_x == 0:
            a = 0
        else:
            a = delta_y / delta_x
        b = y1 - a * x1
        step = int(delta_x/abs(delta_x))
        for x in range(x1, x2+step, step):
            y = round(a * x + b)
            if x > self.width or x < 0 or \
               y > self.height or y < 0:
                continue
            self.buffer[y][x] = color

    # y = ax + b
    def draw_circle(self, x, y, r, color, fill=False):
        start_x = max(x - r, 0)
        start_y = max(y - r, 0)
        end_x = min(x + r, self.width)
        end_y = min(y + r, self.height)
        # Add 0.5 is for not point out a pixel
        r2 = round((r+0.5) ** 2)
        for _y in range(start_y, end_y+1):
            for _x in range(start_x, end_x+1):
                dy = _y - y
                dx = _x - x
                _r2 = round(dy**2 + dx**2)
                if abs(_r2 - r2) < 7:
                    self.buffer[_y][_x] = color
                if _r2 < r2 and fill:
                    self.buffer[_y][_x] = color

    def draw_bit_map(self, x, y, bit_map, color):
        start_x = max(x, 0)
        start_y = max(y, 0)
        end_x = min(self.width, len(bit_map[0]) + x)
        end_y = min(self.height, len(bit_map) + y)
        for y1 in range(start_y, end_y):
            for x1 in range(start_x, end_x):
                pixel = bit_map[y1-y][x1-x]
                if pixel == "1":
                    self.draw_point(x1, y1, color)

    def draw_text(self, x, y, text, font, color):
        bit_map = self.string_to_string_bits(text, font)
        self.draw_bit_map(x, y, bit_map, color)

#     def scroll_text(self, y, text, font, color, delay=500):
#         bit_map = self.string_to_string_bits(text, font)
#         for i in range(len(bit_map[0]) - self.width):
#             self.draw_bit_map(-i, y, bit_map, color)
#             self.write()
#             time.sleep_ms(delay)
#             self.clear()

    def draw_sprite(self, x, y, sprite):
        if isinstance(sprite, str):
            sprite = self.sprites[sprite]
        start_x = max(x, 0)
        start_y = max(y, 0)
        for _y in range(start_y, self.height):
            if _y >= len(sprite):
                break
            for _x in range(start_x, self.width):
                if _x >= len(sprite[_y]):
                    break
                color = sprite[_y-y][_x-x]
                if color != -1:
                    self.draw_point(_x, _y, color)
                
    def write(self):
        temp = list(self.buffer)
        for y in range(self.height):
            for x in range(self.width):
                pattern_x = x // 8
                pattern_y = y // 8
                pattern_i = self.pattern[pattern_y][pattern_x] - 1
                module_x = x % 8
                module_y = y % 8
                if self.MATRIX_MODE == self.ZIGZAG and\
                   y % 2 == 1:
                    module_i = module_y * 8 + 7 - module_x
                else:
                    module_i = module_y * 8 + module_x
                index = module_i + pattern_i * 64
                values = self.buffer[y][x]
                if sysname in ["esp32", "esp8266"]:
                    values = [int(v*self._brightness/255) for v in values]
                self.np[index] = tuple(values)
        self.np.write()
    
    def color_handler(self, color):
        if isinstance(color, list) and len(color) == 3:
            return color
        elif isinstance(color, int):
            r = color >> 16 & 0xFF
            g = color >> 8 & 0xFF
            b = color >> 0 & 0xFF
            return [r, g, b]
        else:
            raise ValueError("Color must be 24-bit  RGB hex or list of 3 8-bit RGB")

    def string_to_string_bits(self, s, font):
        s = str(s)
        smap = []
        for i in range(self.font_list[font].HEIGHT):
            bits = ''
            for letter in s:
                letter = self.font_list[font].get(letter)
                try:
                    bits += letter[-i -1]
                except:
                    for j in range(len(letter[0])):
                        bits += '0'
                bits += '0'
            smap.append(bits)
        smap.reverse()
        return smap

    def hue2rgb(self, _h, _s = 1, _b = 1):
        _hi = int((_h/60)%6)
        _f = _h / 60.0 - _hi
        _p = _b * (1 - _s)
        _q = _b * (1 - _f * _s)
        _t = _b * (1 - (1 - _f) * _s)
        
        if _hi == 0:
            _R_val = _b
            _G_val = _t
            _B_val = _p
        if _hi == 1:
            _R_val = _q
            _G_val = _b
            _B_val = _p
        if _hi == 2:
            _R_val = _p
            _G_val = _b
            _B_val = _t
        if _hi == 3:
            _R_val = _p
            _G_val = _q
            _B_val = _b
        if _hi == 4:
            _R_val = _t
            _G_val = _p
            _B_val = _b
        if _hi == 5:
            _R_val = _b
            _G_val = _p
            _B_val = _q
        
        r = int(_R_val*255)
        g = int(_G_val*255)
        b = int(_B_val*255)
        return [r,g,b]

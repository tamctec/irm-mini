
Examples
====================================

Arduino single test
-------------------------

This is an example for a single IRM mini. It's from Adafruit_NeoMatrix example with IRM mini settings.

.. code-block:: c

    #include <Adafruit_GFX.h>
    #include <Adafruit_NeoMatrix.h>
    #include <Adafruit_NeoPixel.h>
    #ifndef PSTR
        #define PSTR // Make Arduino Due happy
    #endif

    #define PIN 6

    Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(8, 8, PIN,
        NEO_MATRIX_TOP     + NEO_MATRIX_LEFT +
        NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
        NEO_GRB            + NEO_KHZ800);

    const uint16_t colors[] = {
        matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(0, 0, 255) };

    void setup() {
        matrix.begin();
        matrix.setTextWrap(false);
        matrix.setBrightness(40);
        matrix.setTextColor(colors[0]);
    }

    int x    = matrix.width();
    int pass = 0;

    void loop() {
        matrix.fillScreen(0);
        matrix.setCursor(x, 0);
        matrix.print(F("Howdy"));
        if(--x < -36) {
            x = matrix.width();
            if(++pass >= 3) pass = 0;
            matrix.setTextColor(colors[pass]);
        }
        matrix.show();
        delay(100);
    }


Arduino tile test
-------------------------

This is an example for a tile of IRM mini. It's from Adafruit_NeoMatrix example with IRM mini settings of 2x2.

.. code-block:: c

    #include <Adafruit_GFX.h>
    #include <Adafruit_NeoMatrix.h>
    #include <Adafruit_NeoPixel.h>
    #ifndef PSTR
    #define PSTR // Make Arduino Due happy
    #endif

    #define PIN 6

    Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(8, 8, 2, 2, PIN,
        NEO_TILE_TOP   + NEO_TILE_LEFT   + NEO_TILE_ROWS   + NEO_TILE_ZIGZAG +
        NEO_MATRIX_TOP + NEO_MATRIX_LEFT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG,
        NEO_GRB + NEO_KHZ800);

    const uint16_t colors[] = {
        matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(0, 0, 255) };

    void setup() {
        matrix.begin();
        matrix.setTextWrap(false);
        matrix.setBrightness(40);
        matrix.setTextColor(colors[0]);
    }

    int x    = matrix.width();
    int pass = 0;

    void loop() {
        matrix.fillScreen(0);
        matrix.setCursor(x, 0);
        matrix.print(F("Howdy"));
        if(--x < -36) {
            x = matrix.width();
            if(++pass >= 3) pass = 0;
            matrix.setTextColor(colors[pass]);
        }
        matrix.show();
        delay(100);
    }

Micropython for ESP8266/ESP32 single test
-------------------------------------
    
.. code-block:: python

    from machine import Pin
    from ws2812 import WS2812
    import time
    
    ws = WS2812(Pin(0), 64)
    
    ws.brightness = 20
    
    RED   = [255, 0, 0]
    GREEN = [0, 255, 0]
    BLUE  = [0, 0, 255]
    WHITE = [0, 0, 255]
    
    for i in range(len(ws)):
        ws[i] = RED
        ws.write()
        time.sleep(0.05)
    for i in range(len(ws)):
        ws[i] = GREEN
        ws.write()
        time.sleep(0.05)
    for i in range(len(ws)):
        ws[i] = BLUE
        ws.write()
        time.sleep(0.05)
    for i in range(len(ws)):
        ws[i] = WHITE
        ws.write()
        time.sleep(0.05)
    
Micropython for Raspberry Pi Pico single test
-------------------------------------

.. code-block:: python

    from machine import Pin
    from ws2812 import WS2812
    import time
    
    ws = WS2812(Pin(0), 64)
    
    ws.brightness = 20
    
    RED   = [255, 0, 0]
    GREEN = [0, 255, 0]
    BLUE  = [0, 0, 255]
    WHITE = [0, 0, 255]
    
    for i in range(len(ws)):
        ws[i] = RED
        ws.write()
        time.sleep(0.05)
    for i in range(len(ws)):
        ws[i] = GREEN
        ws.write()
        time.sleep(0.05)
    for i in range(len(ws)):
        ws[i] = BLUE
        ws.write()
        time.sleep(0.05)
    for i in range(len(ws)):
        ws[i] = WHITE
        ws.write()
        time.sleep(0.05)


..         from irm import IRM
.. from machine import Pin
.. import time

.. pin = Pin(0, Pin.OUT)

.. irm = IRM(pin, [[1]])
.. irm.brightness = 20

.. irm.fill([0, 0, 0])

.. for i in range(8, -70, -1):
..     irm.fill([0, 0, 0])
..     irm.draw_text(i, 2, "Oh hello there!", irm.FONT5, [0, 30, 0])
..     irm.write()
..     time.sleep(0.1)

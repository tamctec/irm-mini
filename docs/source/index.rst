IRM mini
====================================

.. image:: ../images/irm-mini-front.png

Infinity RGB matrix mini, an 8 by 8 RGB matrix with possibility to extend "infinitely". The idea of IRM mini is to have connectors on 4 side, for you to connect it with multiple unit of IRM mini, to easlily and neetly expand it as your wish.

.. toctree::
   :maxdepth: 3

   hardware
   software
   examples
   ws2812_reference

WS2812 References
================================================================

Class
----------------------------

``WS2812(pin, num)``: A class to control ws2812 RGB LED for RP2040 chip

.. code-block:: python

    from machine import Pin
    from ws2812 import WS2812
    ws = WS2812(Pin(0), 64)

Methods
--------------------------------

``WS2812.__setitem__()``: Set led color buffer

.. code-block:: python

    # Set first LED to Red
    ws[0] = [100, 0, 0]
    # Set second LED to Green
    ws[1] = 0x006400
    # Set the rest LED to Blue
    for i in range(2, len(ws)):
        ws[i] = [0, 0, 100]
    ws.write()

``WS2812.__getitem__()``: read led color buffer

.. code-block:: python

    print(ws[0])

``WS2812.write()``: Write buffer to LEDs

.. code-block:: python

    # Write all LED to white at ones
    for i in range(len(ws)):
        ws[i] = [20, 20, 20]
    ws.write()

    # Write LED to red one at a time
    import time
    for i in range(len(ws)):
        ws[i] = [20, 20, 20]
        ws.write()
        time.sleep(0.1)

Constants
-------------------

``WS2812.brightness``: Brightness for all LEDs, from 0 to 255
``WS2812.length``: Length of LEDs
``WS2812.pin``: the pin of LEDs

Software
=============


IRM mini uses WS2812, addressable RGB LED, or NeoPixel from Adafruit.
So there are tons of libraries for it.
Libraries from Adafruit have solutions of WS2812 matrix, and matrix of led matrix module, which is perfect for IRM mini and so it is recommended.
Here we will mention some of the usage and configurations. But you can also use other libraries,
and change configs according to the :ref:`hardware`

Arduino
----------------------------------------------------------------

.. warning::
  Before we recommanded to use `Adafruit_NeoPixel <https://github.com/adafruit/Adafruit_NeoPixel>`_ and `Adafruit_NeoMatrix <https://github.com/adafruit/Adafruit_NeoMatrix>`_ but as a result, Adafruit Neo Matrix Library flip every zigzag line for easy wiring for other displays. But for IRM mini, we did not flip the line, so we fork the Adafruit_NeoMatrix and adapt it for IRM mini.

For Arduino install IRM Mini Arduino Library

Open Arduino IDE, click through `Sketch`, `Include Library`, `Manage Libraries...`. Search IRM mini, and install it.

Or manually download from GitHub: https://github.com/TAMCTec/irm-mini-arduino

**Step 1**: under https://github.com/TAMCTec/irm-mini-arduino, click `Clone` then `Download ZIP`

**STEP 2**: DO NOT unzip the ZIP file, goto Arduino IDE, click through `Sketch`, `Include Library`, `Add .ZIP Library`, and choose the donloaded ZIP file.

**Step 3**: Now open an NeoMatrix example and change matrix settings to match your IRM mini.

.. code-block:: c

    // Change pin you are connecting to
    #define PIN 13

    // Change the layout of the matrix
    #define TILE_WIDTH 6
    #define TILE_HEIGHT 2


Micropython
----------------------------------------------------------------

For ESP32 and ESP8266

.. code-block:: python

    import machine, neopixel

    n = 64
    PIN = 5

    np = neopixel.NeoPixel(machine.Pin(PIN), n)

Checkout `Controlling NeoPixels <https://docs.micropython.org/en/latest/esp8266/tutorial/neopixel.html#controlling-neopixels>`_

For Raspberry Pi Pico or other RP2040 boards


CircuitPython
----------------------------------------------------------------

